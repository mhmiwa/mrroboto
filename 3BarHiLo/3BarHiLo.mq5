//+------------------------------------------------------------------+
//|                                                     3BarHiLo.mq5 |
//|                        Copyright 2021, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
#include <Trade\Trade.mqh>

double bufferMA20[];
double bufferMA3Low[];
double bufferMA3High[];
int handlerMA20;
int handlerMA3Low;
int handlerMA3High;

int OnInit()
  {
//---
   handlerMA20 = iMA(Symbol(), Period(), 20, 0, MODE_SMA, PRICE_CLOSE);
   handlerMA3Low = iMA(Symbol(), Period(), 3, 0, MODE_SMA, PRICE_LOW);
   handlerMA3High = iMA(Symbol(), Period(), 3, 0, MODE_SMA, PRICE_HIGH);
//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//---
   
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
{
  if(ShouldBuy())
  {
      Buy();
  }
   
  if(ShouldStop())
  {
      Stop();
  }
}

bool ShouldBuy()
{
    if(PositionsTotal() == 0 && IsMA20Ascending())
    {
       
       CopyBuffer(handlerMA3Low, 0, 1, 1, bufferMA3Low);
       
       double current_ask = SymbolInfoDouble(_Symbol, SYMBOL_ASK); 
       
       return bufferMA3Low[0] >= current_ask;
    }
    return false;
}



bool IsMA20Ascending()
{
    
    CopyBuffer(handlerMA20, 0, 1, 6, bufferMA20);
    return bufferMA20[3] > bufferMA20[2] && bufferMA20[4] > bufferMA20[3] && bufferMA20[5] > bufferMA20[4];
}

void Buy()
{
   CTrade trade;
   trade.Buy(100,NULL,0.0,0.0,0.0,"Comprado");
}

bool ShouldStop()
{
   if(PositionsTotal())
   {
       CopyBuffer(handlerMA3High, 0, 1, 1, bufferMA3High);
       
       double current_bid = SymbolInfoDouble(_Symbol, SYMBOL_BID); 
       
       return bufferMA3High[0] <= current_bid;
   }
   return false;
}

void Stop()
{
   CTrade trade;
   trade.PositionClose(Symbol(),-1);
}